package boot;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BootController {
	
	@RequestMapping("/")
	public String indexfunc(Map<String,Object> map)
	{
		map.put("msg","Welcome To Spring Boot ");
		return "index";
	}
	

	@RequestMapping("/next")
	public String nextfunc(Map<String,Object> map)
	{
		map.put("msg","Hii User ");
		return "next";
	}
	
    @Value("${welcome.message}")
    private String messages = "";

    @RequestMapping("/msg")
    public String welcome(Map<String, Object> model) {
                    model.put("message",messages);
                    return "welcome";

    }
}
